#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
int convertiraccent(wchar_t test, int i){
	if(test==L'é' ||  test==L'è' ||  test==L'ê' || test==L'ë'){
				return 1;}

	else if(test==L'à' ||  test==L'á' || test==L'â' || test==L'ã'|| test==L'ä' || test==L'å'){
				return 2;
	}

	else if(test==L'ì' ||  test==L'í' || test==L'î' || test==L'ï'){
				return 3; }

	else if(test==L'ò' ||  test==L'ó' || test==L'ô' || test==L'õ' || test==L'õ' || test==L'ö'){				return 4;}

	else if(test==L'ú' ||  test==L'û' || test==L'ù' || test==L'õ' || test==L'ü'){
				return 5;}
	
}
int nonautoriser(wchar_t test){
	wchar_t tab_interdit[]={L'×',L',',L'Þ',L'ß',L'æ',L'Æ',L'þ',L'ð',L'+'};
	for(int i=0;i<wcslen(tab_interdit);i++){
		if(test==tab_interdit[i]){
			return 0;
		}
	}
}

void verifieralphanumerique(wchar_t* saisi,wchar_t* bon_tab){

	wchar_t valeur_alphabet;
	wchar_t tab_alphabetm[]={L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
	wchar_t tab_alphabet[]={L"abcdefghijklmnopqrstuvwxyz"};
	int test=0;
	for(int i=0;i<wcslen(saisi)-1;i++){
		if(nonautoriser(saisi[i])!=0){
			for(int j=0;j<wcslen(tab_alphabetm);j++){
				if(saisi[i]==tab_alphabetm[j]){
					bon_tab[i]=tab_alphabet[j];
					wprintf(L"le bon tableau est %lc\n",bon_tab[i]);
					test=1;
					
				}
			}
			convertiraccent(saisi[i], i);
			if(convertiraccent(saisi[i], i)==1){

				bon_tab[i]=tab_alphabet[4];
				test=3;
			}
			else if(convertiraccent(saisi[i], i)==2){

				bon_tab[i]=tab_alphabet[0];
				test=3;
			}
			else if(convertiraccent(saisi[i], i)==3){

				bon_tab[i]=tab_alphabet[8];
				test=3;
			}
			else if(convertiraccent(saisi[i], i)==4){
				bon_tab[i]=tab_alphabet[14];
				test=3;
			}
			else if(convertiraccent(saisi[i], i)==5){
				bon_tab[i]=tab_alphabet[20];
				test=3;
			}
			else if(test==0){
				bon_tab[i]=saisi[i];;
				
			}
			test=0;
		
		
	}
	
}
}

void cesar(wchar_t* teste_message){
	//variable
	int i=0;
	wchar_t tab_alphabet[]={L"abcdefghijklmnopqrstuvwxyz"};
	int clef;
	wchar_t message_crypte[200]={0};
	/*
	wprintf(L"les valeur du mot sont : \n");
	while(i<wcslen(teste_message) && teste_message[i]!='\n'){
		wprintf(L"le contenu du tableau est %c \n\n",teste_message[i]);
		i++;
	}
	*/
	//on demande la clef
	wprintf(L"-------- Saisir votre clef ------- \n");
    	wscanf(L"%ld",&clef);
	wprintf(L"la valeur de votre clef est %ld\n\n",clef);
	int cleff=clef;
	
	//on traduit les caractere si il y en a et on
	for(int i=0;i<wcslen(teste_message);i++){
		if(teste_message[i]==' '){
			message_crypte[i]=' ';
		}
		else if(teste_message[i]!='\n'){
			int j=0;
			while(teste_message[i]!=tab_alphabet[j]){
				j+=1;
			}
			int somme=j+cleff;
			if(somme>=26){
				int bon_nombre=somme%26;
				message_crypte[i]=tab_alphabet[bon_nombre];
				somme=0;
			}
			else{
				message_crypte[i]=tab_alphabet[somme];
				somme=0;
				
			}
		}
	}

	//on affiche le message crypte
	wprintf(L"------- le message crypte est : %ls -------- \n",message_crypte);
}
//a faire
void vigenere(wchar_t* teste_message){
	
	//variables
	wchar_t tab_alphabet[]={L"abcdefghijklmnopqrstuvwxyz"};
	wchar_t message_vivi[200]={0};
	int tab_valeur_tmessage[200]={0};
	int tab_valeur_mvivi[200]={0};
	int somme_tab[200]={0};
	int v=0;
	int j=0;
	int i=0;
	wchar_t clef[200];
	int somme[200]={0};
	wchar_t bonmessage[200]={0};

	//on demande la clef
	wprintf(L"-------- Saisir votre clef ------- \n");
    	wscanf(L"%ls",&clef);
	wprintf(L"#la valeur de votre clef est %ls\n\n",clef);

	//on prend la longueur de la clef
	int longeur_clef=wcslen(clef);

	while(i<wcslen(teste_message) && teste_message[i]!='\n'){
		i++;
	}
	
	
	//on dublique la clef en codant de la meme maniere que le message rentrer par l'utilisateur
	while(v<i){
		if(teste_message[v]==' '){
			message_vivi[v]=' ';
			
		}
		else{
			if(j<longeur_clef){
			message_vivi[v]=clef[j];
				j++;
			}
			else{
				j=0;
				message_vivi[v]=clef[j];
				j++;
			}
			
		}
		v++;
	}
	
	int compteur=0;

	//on cherche l'indice de chaque lettre  du message dans le tableau alphabet et on stocke
	for(int k=0;k<i;k++){
		int h=0;
		if(teste_message[k]==' '){
			tab_valeur_tmessage[k]=-1;
			
		compteur++;
		}
		else{
			while(teste_message[k]!=tab_alphabet[h]){
				h++;
			}
			tab_valeur_tmessage[k]=h;
			
		compteur++;
		}
		
	}
	//on cherche l'indice de chaque lettre  de la clef dans le tableau alphabet et on stocke
	for(int a=0;a<i;a++){
		int b=0;
		if(message_vivi[a]==' '){
			tab_valeur_mvivi[a]=-1;
			
		}
		else{
			while(message_vivi[a]!=tab_alphabet[b]){
			b++;
			}
			tab_valeur_mvivi[a]=b;
			
		}
		
	}
	
	// on fait l addition
	for(int w=0;w<compteur;w++){
		if(tab_valeur_tmessage[w]==-1){
			somme[w]=-1;
			
		}
		else{
			somme[w]=tab_valeur_tmessage[w]+tab_valeur_mvivi[w];
			if(somme[w]>=26){
				somme[w]=somme[w]%26;
				
			}
		}
	}
	
	//on stocke dans le tableau 
	for(int s=0;s<compteur;s++){
		if(somme[s]==-1){
			bonmessage[s]=' ';
		}
		else{
		bonmessage[s]=tab_alphabet[somme[s]];
		}
	}
	
	//on affiche
	wprintf(L"------ Le message crypte est  %ls ------\n",bonmessage);

}
void vigenere_dechiffrement(wchar_t* teste_message){

	//variables
	wchar_t tab_alphabet[]={L"abcdefghijklmnopqrstuvwxyz"};
	wchar_t message_vivi[200]={0};
	int tab_valeur_tmessage[200]={0};
	int tab_valeur_mvivi[200]={0};
	int somme_tab[200]={0};
	int v=0;
	int j=0;
	int i=0;
	wchar_t clef[200];
	int compteur=0;
	int somme[200]={0};
	wchar_t bonmessage[200]={0};

	//on demande la clef
	wprintf(L"-------- Saisir votre clef ------- \n");
    	wscanf(L"%ls",&clef);
	wprintf(L"#la valeur de votre clef est %ls\n\n",clef);

	//on prend la longueur de la clef
	int longeur_clef=wcslen(clef);

	while(i<wcslen(teste_message) && teste_message[i]!='\n'){
		i++;
	}
	
	//on dublique la clef en codant de la meme maniere que le message rentrer par l'utilisateur
	while(v<i){
		if(teste_message[v]==' '){
			message_vivi[v]=' ';
			
		}
		else{
			if(j<longeur_clef){
			message_vivi[v]=clef[j];
				j++;
			}
			else{
				j=0;
				message_vivi[v]=clef[j];
				j++;
			}
			
		}
		v++;
	}
	
	
	//on cherche l'indice de chaque lettre  du message dans le tableau alphabet et on stocke
	for(int k=0;k<i;k++){
		int h=0;
		if(teste_message[k]==' '){
			tab_valeur_tmessage[k]=-1;
		compteur++;
		}
		else{
			while(teste_message[k]!=tab_alphabet[h]){
				h++;
			}
			tab_valeur_tmessage[k]=h;
		compteur++;
		}
		
	}
	//on cherche l'indice de chaque lettre  de la clef dans le tableau alphabet et on stocke
	for(int a=0;a<i;a++){
		int b=0;
		if(message_vivi[a]==' '){
			tab_valeur_mvivi[a]=-1;
		}
		else{
			while(message_vivi[a]!=tab_alphabet[b]){
			b++;
			}
			tab_valeur_mvivi[a]=b;
		}
		
	}
	
	// on fait la soustraction
	for(int w=0;w<compteur;w++){
		if(tab_valeur_tmessage[w]==-1){
			somme[w]=-1;
			
		}
		else{
			somme[w]=tab_valeur_tmessage[w]-tab_valeur_mvivi[w];
			if(somme[w]<0){
				while(somme[w]<0){
					somme[w]=somme[w]+26;
					
				}
			}
			
		}
	}
	//on stocke dans le tableau 
	for(int s=0;s<compteur;s++){
		if(somme[s]==-1){
			bonmessage[s]=' ';
		}
		else{
		bonmessage[s]=tab_alphabet[somme[s]];
		}
	}

	//on affiche
	wprintf(L"------Le message decrypte est: %ls ------\n",bonmessage);
	

}
int main(void)
{
	struct lconv *loc;
	setlocale (LC_ALL, "");
	loc=localeconv();
	
	//variables
	wchar_t message[200];
	wchar_t message_sansmaj[200]={0};
	int demande;

	//demande du message à l'utilisateur
	wprintf(L"-------- Saisir le message ------- \n");
	fgetws(message, 200, stdin);
	
	//on verifie les accents et majuscule
	verifieralphanumerique(message,message_sansmaj);
	wprintf(L"%ls",message_sansmaj);
	//demande à l'utilisateur de faire son choix 
	wprintf(L"\n------ Rentrer  1) cesar  ou 2) vigenere  ou 3) vigenere dechiffrement -------\n");

    	wscanf(L"%ld",&demande);
	//on verifie que l'utilisateur fait un bon choix
	while(demande<1 || demande>3){
		wprintf(L"------ Oups vous ne n'avez pas rentré un bon numéro réessayer -------\n");
		wscanf(L"%ld",&demande);
	}
		
	//on choisi la bonne fonction en fonction de la demande
	if(demande==1){
		cesar(message_sansmaj);
	}
	else if(demande==2){
		vigenere(message_sansmaj);
	}
	else{
		vigenere_dechiffrement(message_sansmaj);
	}
	
}	
//xy scwm lm ayitzyuz
//xy scwm lm ayitzyuz

